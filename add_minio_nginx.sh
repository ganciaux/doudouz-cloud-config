#! /bin/env sh



container_minio=$(docker ps | grep minio_minio | awk '{print $1}')
container_minio2=$(docker ps | grep dcsm_minio | awk '{print $1}')

container_nginx=$(docker ps  | grep nginx | awk '{print $1}')
container_minio_ip=$(docker inspect  -f '{{.NetworkSettings.Networks.proxy.IPAddress}}' $container_minio)
container_minio2_ip=$(docker inspect  -f '{{.NetworkSettings.Networks.proxy.IPAddress}}' $container_minio2)
docker exec $container_nginx ls /etc/nginx/conf.d


echo "
 client_max_body_size 50000m;
# minio.dcsm.ch/
# $container_minio => $container_minio_ip
upstream minio-api.dcsm.ch {
    server $container_minio_ip:9000;
}
server {
    server_name minio-api.dcsm.ch;
    http2 on;
    access_log /var/log/nginx/access.log vhost;
    listen 9000 ssl ;
    ssl_session_timeout 5m;
    ssl_session_cache shared:SSL:50m;
    ssl_session_tickets off;
    ssl_certificate /etc/nginx/certs/minio-api.dcsm.ch.crt;
    ssl_certificate_key /etc/nginx/certs/minio-api.dcsm.ch.key;
    set \$sts_header \"\";
    if (\$https) {
        set \$sts_header \"max-age=31536000\";
    }
    add_header Strict-Transport-Security \$sts_header always;
    location / {
        proxy_pass http://minio-api.dcsm.ch/;
        set \$upstream_keepalive false;
    }
}

# minio2.dcsm.ch/
# $container_minio2 => $container_minio2_ip
upstream minio2-api.dcsm.ch {
    server $container_minio2_ip:9000;
}
server {
    server_name minio2-api.dcsm.ch;
    http2 on;
    access_log /var/log/nginx/access.log vhost;
    listen 9100 ssl ;
    ssl_session_timeout 5m;
    ssl_session_cache shared:SSL:50m;
    ssl_session_tickets off;
    ssl_certificate /etc/nginx/certs/minio2-api.dcsm.ch.crt;
    ssl_certificate_key /etc/nginx/certs/minio2-api.dcsm.ch.key;
    set \$sts_header \"\";
    if (\$https) {
        set \$sts_header \"max-age=31536000\";
    }
    add_header Strict-Transport-Security \$sts_header always;
    location / {
        proxy_pass http://minio2-api.dcsm.ch/;
        set \$upstream_keepalive false;
    }
}

" > my_proxy.conf

docker cp my_proxy.conf $container_nginx:/etc/nginx/conf.d/
docker exec $container_nginx /usr/sbin/nginx -s reload
