docker stack rm portainer
docker run -it -p 80:80 -p 443:443 --rm --name certbot -v $PWD/letsencrypt:/etc/letsencrypt certbot/certbot "$@"

live=~tarantino/doudouz-cloud-config/letsencrypt/live/doudouz-portainer.anciaux.ch/
certs=~tarantino/doudouz-cloud-config/letsencrypt/certs/

for dom in doudouz-portainer.anciaux.ch marie-laure.anciaux.fr minio.dcsm.ch minio-api.dcsm.ch jtcam.dcsm.ch akantu-workshop.dcsm.ch minio2.dcsm.ch minio2-api.dcsm.ch; do 
    cp $live/fullchain.pem $certs/$dom.crt
    cp $live/privkey.pem $certs/$dom.key 
done

bash launch_portainer.sh
